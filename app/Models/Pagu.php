<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagu extends Model
{
    protected $table='pagu';
    protected $fillable =
        [
            'tahun','jumlah_anggaran',
        ];
    public function program()
    {
        return $this->belongsTo(Program::class);
    }
}
