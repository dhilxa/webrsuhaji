<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailItem extends Model
{
    protected $table='detail_item';
    protected $fillable =
        [
            'nama'
        ];
    public function item_bu()
    {
        return $this->belongsTo(ItemBu::class);
    }
}
