<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    protected $table='rekening';
    protected $fillable =
        [
            'no_rek','parent','nama_pj'
        ];
    public function subkegiatan()
    {
        return $this->hasMany(Subkegiatan::class);
    }
    public function kegiatan()
    {
        return $this->hasMany(Kegiatan::class);
    }
}
