<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemBu extends Model
{
    protected $table='item_bu';
    protected $fillable =
        [
            'nama'
        ];
    public function detail_bu()
    {
        return $this->belongsTo(DetailBu::class);
    }
    public function detail_item()
    {
        return $this->hasMany(DetailItem::class);
    }
}
