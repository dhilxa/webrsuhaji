<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table='role';
    protected $fillable =
        [
            'title',
        ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
