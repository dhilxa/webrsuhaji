<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table='unit';
    protected $fillable =
        [
            'nama'
        ];
    public function pptk_unit()
    {
        return $this->belongsTo(PptkUnit::class);
    }
    public function detail_bu()
    {
        return $this->hasOne(DetailBu::class);
    }
}
