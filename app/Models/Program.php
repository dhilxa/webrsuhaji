<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table='program';
    protected $fillable =
        [
            'nama',
        ];
    public function pagu()
    {
        return $this->hasOne(Pagu::class);
    }
    public function kegiatan()
    {
        return $this->hasMany(Kegiatan::class);
    }
    public function user()
    {
        return $this->belongsToMany(User::class);
    }
    public function bid_program()
    {
        return $this->hasMany(BidProgram::class);
    }
}
