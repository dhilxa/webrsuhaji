<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $table='kegiatan';
    protected $fillable =
        [
            'nama','jumlah_anggaran'
        ];
    public function subkegiatan()
    {
        return $this->hasMany(Subkegiatan::class);
    }
    public function rekening()
    {
        return $this->belongsTo(Rekening::class);
    }
    public function program()
    {
        return $this->belongsTo(Program::class);
    }
    public function program_pptk()
    {
        return $this->belongsToMany(ProgramPptk::class);
    }
}
