<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailBu extends Model
{
    protected $table='detail_bu';
    protected $fillable =
        [
            'jumlah_biaya','jumlah_pendapatan'
        ];
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
    public function item_bu()
    {
        return $this->hasMany(ItemBu::class);
    }
}
