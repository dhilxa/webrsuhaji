<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PptkUnit extends Model
{
    protected $table='pptk_unit';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function unit()
    {
        return $this->hasOne(Unit::class);
    }
}
