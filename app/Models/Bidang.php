<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bidang extends Model
{
    protected $table='bidang';
    protected $fillable =
        [
            'title',
        ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bid_program()
        {
            return $this->hasMany(BidProgram::class);
        }
}