<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subkegiatan extends Model
{
    protected $table='subkegiatan';
    protected $fillable =
        [
            'nama','jumlah_anggaran'
        ];
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class);
    }
    public function rekening()
    {
        return $this->belongsTo(Rekening::class);
    }
    public function realisasi()
    {
        return $this->hasMany(Realisasi::class);
    }
}
