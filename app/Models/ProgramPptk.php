<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramPptk extends Model
{
    public function kegiatan()
    {
        return $this->belongsToMany(Kegiatan::class);
    }
}
