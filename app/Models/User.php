<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $fillable = [
        'name', 'email', 'password', 'nip', 'golongan', 'role_id','foto'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pptk_unit()
    {
        return $this->hasMany(PptkUnit::class);
    }

    public function program()
    {
        return $this->belongsToMany(Program::class);
    }

    public function bidang()
    {
        return $this->hasMany(Bidang::class);
    }
}
