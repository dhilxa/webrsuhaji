<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Realisasi extends Model
{
    protected $table='realisasi';
    protected $fillable =
        [
            'triwulan_ke','jumlah_anggaran','jenis'
        ];
    public function subkegiatan()
    {
        return $this->belongsTo(Subkegiatan::class);
    }
}
