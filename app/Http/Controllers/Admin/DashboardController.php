<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $permission = Permission::get();
        $role = Role::get();
        // $role->syncPermissions($permission);
        $user = User::first();
        // $auth = Auth::loginUsingId($user->id);
        // $user->removeRole('Viewer');
        // dd($role);
        dd(Auth::check());
        // Permission::create([
        //     'name' => 'show article'
        // ]);

        $data = [
            'title' => 'Dashboard',
            'subtitle' => 'admin/dashboard',
            'idh' => 'dashboard'
        ];
        return view('admin.pages.dashboard', $data);
    }
}
