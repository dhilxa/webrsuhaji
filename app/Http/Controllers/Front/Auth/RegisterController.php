<?php

namespace App\Http\Controllers\Front\Auth;

use Auth;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nip' => ['required', 'max:25'],
            // 'foto' => ['mimes:jpg'],
            'name' => ['required', 'string', 'max:255'],
            'golongan' => ['required', 'string', 'max:255'],
            // 'jabatan' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], [
            'name.required' => "Masukkan nama panjang anda",
            'email.required' => "Masukkan email anda",
        ]);
    }

    public function showRegisterForm()
    {
        // $roles = Role::all();
        $roles = [];
        return view('front.auth.register', compact('roles'));
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath())->with('status_register', "Berhasil register");
    }

    protected function create(Request $request)
    {
        // if(Input::hasfile($data['foto'])){
        //     $image = Input::file($data['foto']);
        //     $upload = base_path().'/fotoprofil/';
        //     $filename = $data['nip'].'.jpg';
        //     $image->move($upload, $filename);
        //     $path = $upload.$filename;
        // }

        if ($request->has('foto')) {
            $foto = $request->file('foto')->store('foto');
        }

        User::create([
            'nip' => $request->nip,
            'foto' => $foto,
            'name' => $request->name,
            'golongan' => $request->golongan,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('login');
    }

    protected function registered(Request $request, $user)
    {
        // $user->notify(new UserRegisteredNotification($user));
        // notify("Ada user baru", [
        //     'Nama' => $user->nama,
        //     'Email' => $user->email,
        //     'Notelp' => $user->notelp,
        //     'Kota' => $user->kabkota
        // ]);

        // cek global setting untuk tanpa konfirmasi pendaftaran
        // $bypass = (int) Cache::remember('bypass_confirm', 60, function(){
        //     return DB::table('tbl_setting')->where('name', 'bypass_confirm')->first()->value;
        // });
        // if ( $bypass ){
        //     $user->is_verified = 1;
        //     $user->save();
            Auth::loginUsingId($user->id_user);
        // }
    }
}