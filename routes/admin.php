<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by theRouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('admin/dashboard');
});

Route::group(['middleware' => 'guest:admin'], function () {
  Route::get('/login', [
    'uses' => 'Auth\LoginController@showSignin',
    'as' => 'admin.login'
  ]);
});

Route::get('/dashboard', [
  'uses' => 'DashboardController@index',
  'as' => 'admin.dashboard'
]);

Route::group(['middleware' => 'auth:admin'], function () {


});
