<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by theRouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('dashboard'));
});

Route::group(['middleware' => 'guest'], function ()
{
    Route::get('/login', [
        'uses' => 'Auth\LoginController@showLoginForm',
        'as' => 'login'
    ]);

    Route::get('/register', [
        'uses' => 'Auth\RegisterController@showRegisterForm',
        'as' => 'register'
    ]);

    Route::post('/register', [
        'uses' => 'Auth\RegisterController@register',
        'as' => 'register.post'
    ]);
});

Route::group(['middleware' => 'auth'], function ()
{
    Route::get('/dashboard', [
        'uses' => 'HomeController@index',
        'as' => 'dashboard'
    ]);
});



// Route::get('/daftar', 'Auth\RegisterController@showRegister')->name('register');
// Route::get('/signin', 'Auth\LoginController@showSignin')->name('login');
// Route::post('/daftar-akun', 'Auth\RegisterController@create');

