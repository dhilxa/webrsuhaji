<!DOCTYPE html>
<html lang="en" dir="ltr">


<!-- Mirrored from demo.frontted.com/stackadmin/20190201/signup-centered-boxed.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Feb 2019 15:52:10 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Daftar akun</title>
    
    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">
    
    <!-- Simplebar -->
    <link type="text/css" href="{{ asset('assets/vendor/simplebar.min.css') }}" rel="stylesheet">
    
    <!-- App CSS -->
    <link type="text/css" href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/css/app.rtl.css') }}" rel="stylesheet">
    
    <!-- Material Design Icons -->
    <link type="text/css" href="{{ asset('assets/css/vendor-material-icons.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/css/vendor-material-icons.rtl.css') }}" rel="stylesheet">
    
    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href="{{ asset('assets/css/vendor-fontawesome-free.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/css/vendor-fontawesome-free.rtl.css') }}" rel="stylesheet">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133433427-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        
        gtag('config', 'UA-133433427-1');
    </script>
    
    
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '327167911228268');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=327167911228268&amp;ev=PageView&amp;noscript=1" /></noscript>
        <!-- End Facebook Pixel Code -->
        
        
        
        
        
        
    </head>
    
    <body class="layout-login-centered-boxed">
        
        
        
        
        
        <div class="layout-login-centered-boxed__form card">
            <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
                <a href="index.html" class="navbar-brand flex-column mb-2 align-items-center mr-0" style="min-width: 0">
                    <img class="navbar-brand-icon mr-0 mb-2" src="assets/images/stack-logo-blue.svg" width="25" alt="Stack"><br>
                    
                    <span>KEUANGAN RSU HAJI</span>
                </a>
                <p class="m-0">Mohon isikan data terlebih dahulu</p>
            </div>
            
            <form action="{{ url('/daftar-akun') }}" method="POST" enctype="multipart/form-data" novalidate>
                @csrf
                <div class="form-group">
                    <label class="text-label" for="name_2">Nama:</label>
                    <div class="input-group input-group-merge">
                        <input id="name" type="text" name="name" required="" class="form-control form-control-prepended" placeholder="masukkan nama panjang">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="text-label" for="email_2">E-mail:</label>
                    <div class="input-group input-group-merge">
                        <input id="email" type="email" required="" name="email" class="form-control form-control-prepended" placeholder="masukan email">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="text-label" for="password_2">Password:</label>
                    <div class="input-group input-group-merge">
                        <input id="password_2" type="password" name="password" required="" class="form-control form-control-prepended" placeholder="Buat password">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-key"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="text-label" for="nip">NIP:</label>
                    <div class="input-group input-group-merge">
                        <input id="nip" type="text" required="" name="nip" class="form-control form-control-prepended" placeholder="masukkan NIP">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="text-label" for="golongan">Golongan:</label>
                    <div class="input-group input-group-merge">
                        <input id="golongan" type="text" required="" name="golongan" class="form-control form-control-prepended" placeholder="Masukkan golongan contoh : IIIA ">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <label class="text-label" for="golongan">jabatan:</label>
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->title }} </option>
                        @endforeach
                    </select>
                </div>
                
                
                <div class="form-group">
                    <label class="text-label" for="foto">Foto Profil:</label>
                    <div class="input-group input-group-merge">
                        <input type="file" class="form-control" name="foto">
                    </div>
                </div>
                
                <br>
                <br>
                <div class="form-group text-center">
                    <button class="btn btn-primary mb-2" type="submit" >Daftar</button><br>
                    <a href="{{ route('login') }}" >Sudah punya akun?</a> <br>
                </div>
                
            </form>
        </div>
        
        
        <!-- jQuery -->
        <script src="assets/vendor/jquery.min.js"></script>
        
        <!-- Bootstrap -->
        <script src="assets/vendor/popper.min.js"></script>
        <script src="assets/vendor/bootstrap.min.js"></script>
        
        <!-- Simplebar -->
        <script src="assets/vendor/simplebar.min.js"></script>
        
        <!-- DOM Factory -->
        <script src="assets/vendor/dom-factory.js"></script>
        
        <!-- MDK -->
        <script src="assets/vendor/material-design-kit.js"></script>
        
        <!-- App -->
        <script src="assets/js/toggle-check-all.js"></script>
        <script src="assets/js/check-selected-row.js"></script>
        <script src="assets/js/dropdown.js"></script>
        <script src="assets/js/sidebar-mini.js"></script>
        
        <script>
            (function() {
                'use strict';
                // Self Initialize DOM Factory Components
                domFactory.handler.autoInit()
                
                // Connect button(s) to drawer(s)
                var sidebarToggle = document.querySelectorAll('[data-toggle="sidebar"]')
                sidebarToggle = Array.prototype.slice.call(sidebarToggle)
                
                sidebarToggle.forEach(function(toggle) {
                    toggle.addEventListener('click', function(e) {
                        var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer'
                        var drawer = document.querySelector(selector)
                        if (drawer) {
                            drawer.mdkDrawer.toggle()
                        }
                    })
                })
                
                let drawers = document.querySelectorAll('.mdk-drawer')
                drawers = Array.prototype.slice.call(drawers)
                drawers.forEach((drawer) => {
                    drawer.addEventListener('mdk-drawer-change', (e) => {
                        if (!e.target.mdkDrawer) {
                            return
                        }
                        document.querySelector('body').classList[e.target.mdkDrawer.opened ? 'add' : 'remove']('has-drawer-opened')
                        let button = document.querySelector('[data-target="#' + e.target.id + '"]')
                        if (button) {
                            button.classList[e.target.mdkDrawer.opened ? 'add' : 'remove']('active')
                        }
                    })
                })
                
                // SIDEBAR COLLAPSE MENUS
                $('.sidebar .collapse').on('show.bs.collapse', function(e) {
                    e.stopPropagation()
                    var parent = $(this).parents('.sidebar-submenu').get(0) || $(this).parents('.sidebar-menu').get(0)
                    $(parent).find('.open').find('.collapse').collapse('hide');
                    $(this).closest('li').addClass('open');
                });
                $('.sidebar .collapse').on('hidden.bs.collapse', function(e) {
                    e.stopPropagation()
                    $(this).closest('li').removeClass('open');
                });
                
                // ENABLE TOOLTIPS
                $('[data-toggle="tooltip"]').tooltip()
                
                // PRELOADER
                window.addEventListener('load', function() {
                    $('.preloader').fadeOut()
                    domFactory.handler.upgradeAll()
                })
                
            })()
        </script>
        
        <!-- App Settings (safe to remove) -->
        <script src="assets/js/app-settings.js"></script>
        
        
        
        
        
    </body>
    
    
    <!-- Mirrored from demo.frontted.com/stackadmin/20190201/signup-centered-boxed.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Feb 2019 15:52:10 GMT -->
    </html>