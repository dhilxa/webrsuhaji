<div class="mdk-drawer  js-mdk-drawer" id="default-drawer" data-align="start">
    <div class="mdk-drawer__content">
        <div class="sidebar sidebar-light sidebar-left simplebar" data-simplebar>
            <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
                <a href="profile.html" class="flex d-flex align-items-center text-underline-0 text-body">
                    <span class="avatar mr-3">
                        <img src="{{asset('assets/images/avatar/demi.png')}}" alt="avatar"
                            class="avatar-img rounded-circle">
                    </span>
                    <span class="flex d-flex flex-column">
                        <strong>Adrian Demian</strong>
                        <small class="text-muted text-uppercase">Account Manager</small>
                    </span>
                </a>
            </div>
            <div class="sidebar-heading sidebar-m-t">Dashboard</div>
            <ul class="sidebar-menu">
                <li class="sidebar-menu-item {{ $idh=='dashboard'?'active open':'' }}">
                    <a class="sidebar-menu-button" href="{{ route('admin.dashboard') }}">
                        <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">dvr</i>
                        <span class="sidebar-menu-text">Dashboard</span>
                    </a>
                </li>
            </ul>
            <div class="sidebar-heading">User Management</div>
            <div class="sidebar-block p-0">
                <ul class="sidebar-menu" id="components_menu">
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" href="ui-buttons.html">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">mouse</i>
                            <span class="sidebar-menu-text">Role</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
