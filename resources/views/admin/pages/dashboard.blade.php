@extends('admin.layouts.app')

@section('content')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__heading-container">
            <div class="page__heading d-flex align-items-center">
                <div class="flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-2">
                            @for ($i = 0; $i < count($b = explode('/', $subtitle)); $i++)
                                <li class="breadcrumb-item {{ $i == count($b)-1 ? 'active':'' }}"><a href="#">{{ $b[$i] }}</a></li>
                            @endfor
                        </ol>
                    </nav>
                    <h1 class="m-0">{{ $title }}</h1>
                </div>
            </div>
        </div>
        <div class="container-fluid page__container">
            <div class="alert alert-soft-warning d-flex align-items-center card-margin" role="alert">
                <i class="material-icons mr-3">error_outline</i>
                <div class="text-body"><strong>API gateways are now Offline.</strong> Please try the API later. If you want to stay up to date follow our <a href="#">Status Page </a></div>
            </div>
            <div class="row card-group-row">
                <div class="col-lg-4 col-md-6 card-group-row__col">
                    <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center">
                        <div class="flex">
                            <div class="card-header__title text-muted mb-2">Current Target</div>
                            <div class="text-amount">&dollar;12,920</div>
                            <div class="text-stats text-success">31.5% <i class="material-icons">arrow_upward</i></div>
                        </div>
                        <div><i class="material-icons icon-muted icon-40pt ml-3">gps_fixed</i></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 card-group-row__col">
                    <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center">
                        <div class="flex">
                            <div class="card-header__title text-muted mb-2">Earnings</div>
                            <div class="text-amount">&dollar;3,642</div>
                            <div class="text-stats text-success">51.5% <i class="material-icons">arrow_upward</i></div>
                        </div>
                        <div><i class="material-icons icon-muted icon-40pt ml-3">monetization_on</i></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 card-group-row__col">
                    <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center">
                        <div class="flex">
                            <div class="card-header__title text-muted mb-2">Website Traffic</div>
                            <div class="text-amount">8,391</div>
                            <div class="text-stats text-danger">3.5% <i class="material-icons">arrow_downward</i></div>
                        </div>
                        <div><i class="material-icons icon-muted icon-40pt ml-3">perm_identity</i></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="card dashboard-area-tabs" id="dashboard-area-tabs">
                        <div class="card-header p-0 bg-white nav">
                            <div class="row no-gutters flex" role="tablist">
                                <div class="col" data-toggle="chart" data-target="#earningsTrafficChart" data-update='{"data":{"datasets":[{"label": "Traffic", "data":[10,2,5,15,10,12,15,25,22,30,25,40]}]}}' data-prefix="" data-suffix="k">
                                    <a href="#" data-toggle="tab" role="tab" aria-selected="true" class="dashboard-area-tabs__tab card-body text-center active">
                                        <span class="card-header__title m-0">Sessions</span>
                                        <span class="text-amount">18,391</span>
                                    </a>
                                </div>
                                <div class="col border-left" data-toggle="chart" data-target="#earningsTrafficChart" data-update='{"data":{"datasets":[{"label": "Earnings", "data":[7,35,12,27,34,17,19,30,28,32,24,39]}]}}' data-prefix="$" data-suffix="k">
                                    <a href="#" data-toggle="tab" role="tab" aria-selected="false" class="dashboard-area-tabs__tab card-body text-center">
                                        <span class="card-header__title m-0">Orders</span>
                                        <span class="text-amount">&dollar;8,942</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-muted" style="height: 280px;">
                            <div class="chart" style="height: calc(280px - 1.25rem * 2);">
                                <canvas id="earningsTrafficChart">
                                    <span style="font-size: 1rem;"><strong>Website Traffic / Earnings</strong> area chart goes here.</span>
                                </canvas>
                            </div>
                        </div>
                    </div>
                    <div class="card-group">
                        <div class="card card-body text-center">
                            <div class="mb-1"><i class="material-icons icon-muted icon-40pt">assessment</i></div>
                            <div class="text-amount">3,642 </div>
                            <div class="card-header__title mb-2">Visits</div>
                        </div>
                        <div class="card card-body text-center">
                            <div class="mb-1"><i class="material-icons icon-muted icon-40pt">shopping_basket</i></div>
                            <div class="text-amount">&dollar;12,311</div>
                            <div class="card-header__title  mb-2">Purchases</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-header card-header-large bg-white">
                            <h4 class="card-header__title">Revenue by location</h4>
                        </div>
                        <div class="card-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection